<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ActivateController extends Controller
{
    	
    // public function __construct(){
    // 	$this->middleware('auth');
    // }

    protected function verifyaccount(){
    	#get current account ID
    	$this->middleware('auth');
    	$user = auth()->user();

    	$userID = $user['id'];
    	$amount = 1000;


    	#send to Litemore
		$url = 'http://52.1.11.133/mangu_payment/api/order/create/'.$userID.'/'.$amount;

		$result = file_get_contents($url, false);
		$json = json_decode($result, true);

		if($json['responseCode'] == 200) {

			DB::table('users')->where('id', $userID)->update(['status' => 2]);
			return redirect('/home');


		} else {

			//dd('Something didn\'t work');
			return redirect('/home');

		}

    }

    protected function verified(Request $request) {


  		if ($request->isMethod('post')){
  
		   $input = json_decode(file_get_contents('php://input'),true);

		   $response['responseCode'] = 500;
		   $response['message'] = "An error occured";
		   $accno = $input['description'];

		   dd($accno);
		  
		   if ($input['responseCode'] == 200){
		    // successful transaction process the transaction as successful

		   		$response['responseCode'] = 200;
		   		$response['message'] = "Successfully confirmed";
		    	$response['description'] = "";
		    	echo(json_encode($response));

		  	} else if ($input['responseCode'] == 500){
		    	// error occured, process the transaction as unsuccessful
		    	$response['responseCode'] = 500;
		    	$response['message'] = "An error occured";

		    	echo(json_encode($response));
		  	}

		}
    }


}
