<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $result = auth()->user();

        $firstname = $result['firstname'];
        $lastname = $result['lastname'];
        $email = $result['email'];
        $bio = $result['bio'];
        $phone = $result['phone'];
        $house = $result['house'];
        $sport = $result['sport'];
        $status = $result['status'];

        return view('home', compact('firstname','lastname','email','bio','phone', 'house', 'sport', 'status'));
    }
}
