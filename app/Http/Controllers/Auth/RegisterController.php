<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'email-address' => 'required|max:255',
            'lastname' => 'required|max:255',
            'phone' => 'required|max:255',
            'dob' => 'required|max:255',
            'sport' =>'max:255',
            'house' => 'required|max:255',
            'profession' => 'required|max:255',
            'company' => 'required|max:255',
            'position' => 'max:255',
            'bio' => '',
            'status' => ''
 
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'firstname'=> $data['firstname'],
            'lastname'=> $data['lastname'],
            'phone'=> $data['phone'],
            'dob'=> $data['dob'],
            'sport'=> $data['sport'],
            'house'=> $data['house'],
            'email-address'=>$data['email-address'],
            'profession'=> $data['profession'],
            'company'=> $data['company'],
            'position'=> $data['position'],
            'bio'=> $data['bio'],
            'status' => $data['status']

        ]);

        # get user account
        $user = auth()->user();
        dd($user);
        # 

    }
}
