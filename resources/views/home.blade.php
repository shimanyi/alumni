@extends('layouts.app')

@section('content')

    <div class="main-container">
        <section class="bg--secondary space--sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="boxed boxed--lg boxed--border">
                            <div class="text-block text-center">
                                <img alt="avatar" src="img/avatar-round-3.png" class="image--sm" />
                                <span class="h5">{{ $firstname.' '.$lastname }}</span>
                                @if ($status === 0)
                                    <span class="h7">Status: <a href="/verifyaccount" class="no_style"><span style="color:red;">Pending</span></a></span>
                                @elseif ($status === 1)
                                    <span class="h7">Status:<span style="color:green;">Registered</span></a></span>
                            
                                @elseif ($status === 2) 
                                    <span class="h7">Status:<span style="color:green;">Processing</span></a></span>
                            
                                @endif

                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="boxed boxed--lg boxed--border">
                            <div id="account-profile" class="account-tab">
                                <h4>Profile</h4>
                                <form>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>First name</label>
                                            <input type="text" name="name" value="{{ $firstname }}" />
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Last name</label>
                                            <input type="text" name="display-name" value="{{ $lastname }}" />
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Email Address</label>
                                            <input type="email" name="email" value="{{ $email }}" />
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Phone</label>
                                            <input type="text" name="phone" value=" {{ $phone }}" />
                                        </div>
                                        <div class="col-sm-12">
                                            <label>House</label>
                                            <input type="text" name="website" value="{{ $house }}" />
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Sport</label>
                                            <input type="text" name="website" value="{{ $sport }}" />
                                        </div>

                                        <div class="col-sm-12">
                                            <label>Bio</label>
                                            <textarea rows="4" name="bio">{{ $bio }}</textarea>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
            
@endsection
