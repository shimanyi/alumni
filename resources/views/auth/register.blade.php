@extends('layouts.app')

@section ('content')

    <div class="main-container">
    <section class="text-center height-50">
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-sm-8 col-md-6">
                    <h1>Let's hear the rest of it,</h1>
                    <p class="lead">
                        We'd be happy to know all the places you were<br>affliated to in Mang'u
                    </p>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="boxed boxed--border">
                            <form class="text-left" role="form" method="POST" action="{{ route('register') }}">

                                {{ csrf_field() }}

                                <div class="col-sm-12">
                                    <h5>Basic Information</h5>
                                </div>
                                <div class="form-group{{ $errors->has('firstname') ? ' has-error' :''}}">
                                    
                                    <div class="col-md-6">
                                        <label>Firstname</label>
                                        <input id="firstname" type="firstname" class="form-control" name="firstname" value="{{ old('firstname') }}" required>
                                        @if ($errors->has('firstname'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('firstname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('lastname') ? ' has-error' :''}}">
                                    
                                    <div class="col-md-6">
                                        <label>Lastname</label>
                                        <input id="lastname" type="lastname" class="form-control" name="lastname" value="{{ old('lastname') }}" required>
                                        
                                        @if ($errors->has('lastname'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('lastname') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('email-address') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>Email Address</label>
                                        <input id="email-address" type="email-address" class="form-control" name="email-address" value="{{ old('email-address') }}" required>
                                        
                                        @if ($errors->has('email-address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email-address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                

                                <div class="form-group{{ $errors->has('phone') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>Phone</label>
                                        <input id="phone" type="phone" class="form-control" name="phone" value="{{ old('phone') }}" required>
                                        
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('dob') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>Date of Birth</label>
                                        <input id="phone" type="dob" class="form-control" name="dob" value="{{ old('dob') }}" required>
                                        
                                        @if ($errors->has('dob'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('dob') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <h5>Life in Mang'u</h5>
                                </div>
                                <div class="form-group{{ $errors->has('admission') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>Admission No.</label>
                                        <input id="admission" type="admission" class="form-control" name="admission" value="{{ old('admission') }}" required>
                                        
                                        @if ($errors->has('admission'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('admission') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('completion') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>Year of Completion</label>
                                        <div class="input-select">
                                            <select id="completion" type="text" class="form-control" name="completion" value="{{ old('phone') }}" required >
                                                <option selected="" value="Default">Year of Completion</option>
                                                <option value="1925">1925</option>
                                            </select>
                                        </div>
                                        
                                        @if ($errors->has('completion'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('completion') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('sport') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>Sport</label>
                                        <div class="input-select">
                                            <select id="sport" type="text" class="form-control" name="sport" value="{{ old('phone') }}" required >
                                                <option selected="" value="Default">I was launching :)</option>
                                                <option value="legends">Hockey Legends</option>
                                                <option value="wazimba">Wazimba</option>
                                            </select>
                                        </div>
                                        
                                        @if ($errors->has('sport'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('sport') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('house') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>House</label>
                                        <div class="input-select">
                                            <select id="house" type="text" class="form-control" name="house" value="{{ old('phone') }}" required >
                                                <option selected="" value="Default">Select your house</option>
                                                <option value="1925">Fr. Edwards</option>
                                            </select>
                                        </div>
                                        
                                        @if ($errors->has('house'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('house') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <h5>Your Profession</h5>
                                </div>

                                <div class="form-group{{ $errors->has('profession') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>Profession</label>
                                        <input id="profession" type="profession" class="form-control" name="profession" value="{{ old('profession') }}" required>
                                        
                                        @if ($errors->has('profession'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('profession') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('company') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>Company</label>
                                        <input id="company" type="company" class="form-control" name="company" value="{{ old('company') }}" required>
                                        
                                        @if ($errors->has('company'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('company') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('position') ? ' has-error' :''}}">
                                    <div class="col-md-6">
                                        <br>
                                        <label>Position</label>
                                        <input id="position" type="position" class="form-control" name="position" value="{{ old('position') }}" required>
                                        
                                        @if ($errors->has('position'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('position') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('bio') ? ' has-error' :''}}">
                                    <div class="col-md-12">
                                        <br>
                                        <label>Bio</label>
                                        <textarea id="bio" rows="5" name="bio" class="validate-required" value="{{ old('bio') }}"></textarea>
                                        
                                        @if ($errors->has('bio'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('bio') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                
                                <div class="col-sm-12">
                                    <br>
                                    <h5>Account Credentials</h5>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    
                                    <div class="col-md-6">
                                        <label>E-Mail Address</label>
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12"></div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                                    
                                    <div class="col-md-6">
                                        <label>Password</label>
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12"></div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Confirm Password</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="col-md-12"></div>
                                <input hidden name="status" value="0">
                                <div class="col-md-6 col-xs-6">
                                    <button type="submit" class="btn btn--primary type--uppercase">Complete Registration</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    
@endsection