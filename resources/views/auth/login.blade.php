@extends('layouts.app')

@section('content')

    <div class="main-container">
        <section class="height-100 imagebg text-center" data-overlay="4">
            <div class="background-image-holder"></div>
            <div class="container pos-vertical-center">
                <div class="row">
                    <div class="col-sm-7 col-md-5">
                        <h2>Login to continue</h2>
                        <p class="lead">
                            Sign in with your existing Stack account credentials
                        </p>
                        
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login')}}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="col-sm-12">

                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                
                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn--sm btn--primary btn__text type--uppercase">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </form>
                        <span class="type--fine-print block">Don't have an account yet?
                            <a href="{{ url('/register')}}">Create account</a>
                        </span>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
@endsection