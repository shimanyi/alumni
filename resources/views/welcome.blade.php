<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <title>Mang'u Alumni Association</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-rubiklato.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />
    </head>
    <body class=" ">
        <a id="start"></a>
        <div class="nav-container ">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-3 col-sm-2">
                            <a href="index.html">
                                {{-- <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                <img class="logo logo-light" alt="logo" src="img/logo-light.png" /> --}}
                            </a>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </div>
            <!--end bar-->
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs hiddem-sm ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1 col-sm-2 hidden-xs">
                            <div class="bar__module">
                                <a href="index.html">
                                    {{-- <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                    <img class="logo logo-light" alt="logo" src="img/logo-light.png" /> --}}
                                </a>
                            </div>
                            <!--end module-->
                        </div>  
                        <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm">
                            
                             @if (Route::has('login'))
                                    @if (Auth::check())
                                        <a class="btn btn--sm type--uppercase" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <span class="btn__text">
                                               Logout
                                            </span>
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                    @else
                                        <div class="bar__module">
                                            <a class="btn btn--sm type--uppercase" href="{{ url('/login') }}">
                                                <span class="btn__text">
                                                   Sign In
                                                </span>
                                            </a>
                                            <a class="btn btn--sm btn--primary type--uppercase" href="{{ url('/register') }}">
                                                <span class="btn__text">
                                                    Sign Up
                                                </span>
                                            </a>
                                        </div>
                                    @endif 
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </nav>
            <!--end bar-->
        </div>
        <div class="main-container">
            <section class="cover height-50 imagebg" data-overlay="3">
                <div class="background-image-holder">
                    <img alt="background" src="img/alumni.png" />
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="h1--large">
                               Mang'u Alumni Association
                            </h1>
                            {{-- <a class="btn btn--primary type--uppercase" href="#">
                                <span class="btn__text">
                                    View Our Agenda
                                </span>
                            </a> --}}
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="unpad elaborate-form-1">
                <div class="row row--gapless">
                    <div class="col-sm-6 height-50 bg--primary">
                        <div class="pos-vertical-center clearfix">
                            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                                <span class="h1">Join the Association</span>
                                <p class="lead">
                                    By joining the alumni association, you gain access to a huge network
                                    of alumni professionals, access to mentorship programs as well as 
                                    Students' Scholarship support plans.
                                </p>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 height-50 bg--primary-1">
                        <div class="pos-vertical-center clearfix">
                            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                                <span class="h1">Sign Up</span>
                                {{-- <p class="lead">
                                    Create an account
                                </p> --}}
                                <form>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" name="firstname" placeholder="Firstname" />
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="lastname" placeholder="Lastname" />
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="email" name="email" placeholder="Email Address" />
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            {{-- <input type="text" name="completion" placeholder="e.g 1925" /> --}}
                                            <div class="input-select">
                                                <select>
                                                    <option selected="" value="Default">Year of Completion</option>
                                                    <option value="1925">1925</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <button type="submit" class="btn btn--primary">Next</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <footer class="space--sm footer-2 bg--light ">
                <div class="container">
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="type--fine-print">&copy;
                                <span class="update-year"></span> Mang'u Alumni Association</span>
                            <a class="type--fine-print" href="#">By Sisitech Inc.</a>
                        </div>
                        <div class="col-sm-6 text-right text-left-xs">
                            <ul class="social-list list-inline list--hover">
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-linkedin icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li> 
                            </ul>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/flickity.min.js"></script>
        <script src="js/easypiechart.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/typed.min.js"></script>
        <script src="js/datepicker.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/ytplayer.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/granim.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
